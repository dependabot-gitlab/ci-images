#!/bin/bash

set -eu

TARGETARCH="${TARGETARCH:-amd64}"

curl --fail --location --output /tmp/helm.tar.gz "https://get.helm.sh/helm-${HELM_VERSION}-linux-${TARGETARCH}.tar.gz"

mkdir -p "/usr/local/helm-${HELM_VERSION}"
tar -xzf /tmp/helm.tar.gz -C "/usr/local/helm-${HELM_VERSION}"
ln -s "/usr/local/helm-${HELM_VERSION}/linux-${TARGETARCH}/helm" /usr/local/bin/helm && helm version --short
rm -f /tmp/helm.tar.gz

helm plugin install https://github.com/chartmuseum/helm-push
helm plugin install https://github.com/databus23/helm-diff
helm plugin install https://github.com/mbenabda/helm-local-chart-version
