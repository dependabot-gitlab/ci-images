#!/bin/bash

set -eu

TARGETARCH="${TARGETARCH:-amd64}"
HELM_ARCH=${TARGETARCH}
SEMVER_VERSION="$(echo $HELMDOCS_VERSION | grep -oP 'v\K[0-9.]+')"

if [ "${TARGETARCH}" = "amd64" ]; then HELM_ARCH="x86_64"; fi

curl --fail --location --output /tmp/helmdocs.tar.gz \
  "https://github.com/norwoodj/helm-docs/releases/download/${HELMDOCS_VERSION}/helm-docs_${SEMVER_VERSION}_Linux_${HELM_ARCH}.tar.gz"

tar -xzf /tmp/helmdocs.tar.gz helm-docs
mv helm-docs /usr/local/bin/ && helm-docs --version
rm -rf /tmp/helmdocs.tar.gz helm-docs
