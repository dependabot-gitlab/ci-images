#!/bin/bash

set -eu

SEMVER_VERSION="$(echo $DOCKER_VERSION | grep -oP 'v\K[0-9.]+')"

curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] \
  https://download.docker.com/linux/debian bullseye stable" | tee /etc/apt/sources.list.d/docker.list >/dev/null

apt-get update && apt-get install --no-install-recommends -y \
  docker-ce="5:${SEMVER_VERSION}*" \
  docker-ce-cli="5:${SEMVER_VERSION}*" \
  docker-compose-plugin \
  docker-buildx-plugin \
  skopeo

docker --version
docker buildx version
docker compose version

apt-get autoremove -y && apt-get clean -y && rm -rf /var/lib/apt/lists/*
