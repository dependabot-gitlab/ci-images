#!/bin/bash

set -eu

curl -fsSL https://deb.nodesource.com/setup_${NODE_VERSION}.x | bash -

apt-get update && apt-get install --no-install-recommends -y nodejs
node --version
apt-get autoremove -y && apt-get clean -y && rm -rf /var/lib/apt/lists/*
