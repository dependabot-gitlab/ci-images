#!/bin/bash

set -eu

TARGETARCH="${TARGETARCH:-amd64}"
SEMVER_VERSION="$(echo $TERRAFORM_VERSION | grep -oP 'v\K[0-9.]+')"

curl --fail --location --output /tmp/terraform.zip \
  "https://releases.hashicorp.com/terraform/${SEMVER_VERSION}/terraform_${SEMVER_VERSION}_linux_${TARGETARCH}.zip"

unzip -j /tmp/terraform.zip terraform -d /usr/local/bin
rm -rf /tmp/terraform.zip

curl --fail --location --output /usr/local/bin/gitlab-terraform \
  "https://gitlab.com/gitlab-org/terraform-images/-/raw/88f4ad0c22470d65bf56aadb10fce1ec7e383d33/src/bin/gitlab-terraform.sh"

chmod +x /usr/local/bin/gitlab-terraform

gitlab-terraform -version
