#!/bin/bash

set -eu

npm install -g playwright@${PLAYWRIGHT_VERSION}
npx playwright-core install-deps chromium
npm uninstall -g playwright
