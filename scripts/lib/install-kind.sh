#!/bin/bash

set -eu

TARGETARCH="${TARGETARCH:-amd64}"

curl --fail --location --output kind https://kind.sigs.k8s.io/dl/${KIND_VERSION}/kind-linux-${TARGETARCH}
chmod +x kind
mv kind /usr/local/bin/ && kind --version
