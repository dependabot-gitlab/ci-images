#!/bin/bash

set -eu

TARGETARCH="${TARGETARCH:-amd64}"

KUBECONFORM_SOURCE="https://github.com/yannh/kubeconform/releases/download/${KUBECONFORM_VERSION}/kubeconform-linux-${TARGETARCH}.tar.gz"

curl --show-error --fail --location --output /tmp/kubeconform.tar.gz "${KUBECONFORM_SOURCE}"
tar -xf /tmp/kubeconform.tar.gz kubeconform
mv kubeconform /usr/local/bin/ && kubeconform -v
rm -rf /tmp/kubeconform.tar.gz
