# gogs-smocker

Setup for docker image of [Gogs](https://github.com/gogs/gogs) and [Smocker](https://github.com/Thiht/smocker) to be able to run git server with additional mock server for testing purposes.

- `data` folder contains the database for Gogs with initial root user `root` and password `root` and api token `149f5d3c282198a31d3e65a1fa4c1e19085d7930`.
- `certs` folder contains self-signed certificate for HTTPS.
