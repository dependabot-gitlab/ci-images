FROM debian:bookworm-slim

ARG TARGETARCH

ENV DEBIAN_FRONTEND=noninteractive

# Install base dependencies
RUN set -eux; \
  apt-get update && apt-get install --no-install-recommends -y \
  ca-certificates \
  curl \
  gnupg \
  git \
  build-essential \
  jq \
  python3 \
  yamllint \
  time \
  unzip \
  idn2 \
  libyaml-dev \
  rsync; \
  \
  apt-get autoremove -yq; \
  apt-get clean -yqq; \
  rm -rf /var/lib/apt/lists/*

COPY /scripts/lib /scripts

# Install gsutil
ARG GSUTIL_VERSION
RUN if [ -n "$GSUTIL_VERSION" ]; then /scripts/install-gsutil.sh; fi

# Install node
ARG NODE_VERSION
RUN if [ -n "$NODE_VERSION" ]; then /scripts/install-node.sh; fi

# Install snyk
ARG SNYK_VERSION
RUN if [ -n "$SNYK_VERSION" ]; then /scripts/install-snyk.sh; fi

# Pre-install chromium deps
ARG PLAYWRIGHT_VERSION
RUN if [ -n "$PLAYWRIGHT_VERSION" ]; then /scripts/install-chromium-deps.sh; fi

# Install Helm
ARG HELM_VERSION
RUN if [ -n "$HELM_VERSION" ]; then /scripts/install-helm.sh; fi

# Install helmdocs
ARG HELMDOCS_VERSION
RUN if [ -n "$HELMDOCS_VERSION" ]; then /scripts/install-helmdocs.sh; fi

# Install docker
ARG DOCKER_VERSION
RUN if [ -n "$DOCKER_VERSION" ]; then /scripts/install-docker.sh; fi

# Install kubeval
ARG KUBECONFORM_VERSION
RUN if [ -n "$KUBECONFORM_VERSION" ]; then /scripts/install-kubeconform.sh; fi

# Install kind
ARG KIND_VERSION
RUN if [ -n "$KIND_VERSION" ]; then /scripts/install-kind.sh; fi

# Install terraform
ARG TERRAFORM_VERSION
RUN if [ -n "$TERRAFORM_VERSION" ]; then /scripts/install-terraform.sh; fi
